const functions = require("firebase-functions")
const admin = require('firebase-admin')

const user = require('./user');
const image = require("./image");
const work = require('./work')

admin.initializeApp()
    
exports.user = functions
    .region('europe-west1')
    .https.onRequest(user)

exports.image = functions
    .region('europe-west1')
    .https.onRequest(image)
    
exports.work = functions
.region('europe-west1')
.https.onRequest(work)

