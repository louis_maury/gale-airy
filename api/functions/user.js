const admin = require('firebase-admin');

const createUser = (userId, request, response) => {
    const userCollection = admin.firestore().collection('Users')
    const body = request.body

    userCollection
        .doc(userId)
        .set(body)
        .then(() => response.send(body))
        .catch((err) => {
            functions.logger.error('Creating user failed', err)
            response
                .status(500)
                .send("Failed to create user")
        })
}

const updateUser = (userId, request, response) => {
    const userCollection = admin.firestore().collection('Users')
    const body = request.body

    userCollection
        .doc(userId)
        .set(body, { merge: true })
        .then(() => response.send(body))
        .catch((err) => {
            functions.logger.error('Creating user failed', err)
            response
                .status(500)
                .send("Failed to create user")
        })
}

const getUser = (request, response) => {
    const userId = request.query.userId

    try {
        admin
            .firestore()
            .collection('Users')
            .get()
            .then((snapshot) => {
                const result = [];
                snapshot.forEach((doc) => {
                    const data = doc.data()
                    if (data.userId === userId) {
                        result.push(data)
                    }
                });
                response.send(result)
            })
    } catch (err) {
        functions.logger.error('/users failed', err)
        response.send(err.message)
    }
}

const user = (request, response) => {
    const userId = request.query.userId
    if (!userId) {
        response.send('UserId is missing')
    } else {
        switch (request.method) {
            case "GET":
                getUser(request, response)
                break;
            case "POST":
                createUser(userId, request, response)
                break;
            case "PATCH":
                updateUser(userId, request, response)
                break;
            case "DELETE":
        
                break;
            default:
                response.send('Unsupported method')
                break;
        }
    }
}

module.exports = user