const admin = require('firebase-admin');
const { user } = require('firebase-functions/v1/auth');

const createWork = (workId, request, response) => {
    const workCollection = admin.firestore().collection('Works')
    const body = request.body
    workCollection
        .doc(workId)
        .set(body)
        .then(() => response.send(body))
        .catch((err) => {
            functions.logger.error('Creating work failed', err)
            response
                .status(500)
                .send("Failed to create work")
        })
}

const updateWork = (workId, request, response) => {
    const userCollection = admin.firestore().collection('Works')
    const body = request.body

    userCollection
        .doc(workId)
        .set(body, { merge: true })
        .then(() => response.send(body))
        .catch((err) => {
            functions.logger.error('Creating work failed', err)
            response
                .status(500)
                .send("Failed to create work")
        })
}

const getWork = (request, response) => {
    let userId = request.query.userId
    if(userId == "all"){
        try {
            admin
                .firestore()
                .collection('Works')
                .get()
                .then((snapshot) => {
                        const result = [];
                        snapshot.forEach((doc) => {
                            const data = doc.data()
                            result.push(data)
                        });
                        response.send(result)
                    })
        } catch (err) {
            functions.logger.error('/works failed', err)
            response.send(err.message)
        }
    }else{
        try {
            admin
                .firestore()
                .collection('Works')
                .where("userId", "==", userId)
                .get()
                .then((snapshot) => {
                    const result = [];
                    snapshot.forEach((doc) => {
                        const data = doc.data()
                        result.push(data)
                    });
                    response.send(result)
                })
        } catch (err) {
            functions.logger.error('/works failed', err)
            response.send(err.message)
        }
    }
    
}

const work = (request, response) => {
    const workId = request.query.workId
        switch (request.method) {
            case "GET":
                getWork(request, response)
                break;
            case "POST":
                createWork(workId, request, response)
                break;
            case "PATCH":
                updateWork(workId, request, response)
                break;
            case "DELETE":
        
                break;
            default:
                response.send('Unsupported method')
                break;
        }
}

module.exports = work