const admin = require('firebase-admin');

const getImage = async (request, response) => {
    let body = request.body
    const userId = request.query.userId
    const name = request.query.name
    const folder = request.query.folder

    const storage = admin.storage().bucket('gs://gale-airy.appspot.com')
    let file = storage.file( userId + '/' + folder + '/' + name)

    file.download().then(e => response.send(e))
} 

const createImage = (request, response) => {

    let body = request.body
    let name = request.body.name    // "name": "image.png"
    let data = request.body.data    // "data": "{{ La base64 de l'image  }}"
    let path = request.body.path    // "path": "{{ ID du profile }}/{{ Type de photo }}/"


    const storage = admin.storage().bucket('gs://gale-airy.appspot.com')
    storage.file(path + name).save(new Buffer.from(data, 'base64'))
    .then(() => {
        response.send(body)
        storage.file(path + name).makePublic()
    })
    .catch((err) => { response.send(err.message) })
    
}

const image = (request, response) => {
    switch (request.method) {
        case "GET":
            getImage(request, response)
            break;
        case "POST":
            createImage(request, response)
            break;
        case "PATCH":

            break;
        case "DELETE":
    
            break;
        default:
            response.send('Unsupported method')
            break;
    }
}

module.exports = image