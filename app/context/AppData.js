import React, { useState, createContext} from "react"
import AsyncStorage from "@react-native-async-storage/async-storage";
import { getUser, updateUser, createUser, createImage, createWork } from "./api"
import { Alert } from "react-native";
import { signUserIn, signUserOut, signUserUp } from "../firebase";
import Toast from 'react-native-simple-toast'
const UserIdKey = "USERID_KEY";
import uuid from 'react-native-uuid';

export const GlobalContext = createContext({
    user: null,
    isLogged: false,
    login: () => {},
    register: () => {},
    logout: () => {},
    updateUserProfile: () => {},   
    oeuvre: [],
    userid: "",
    AddWork: () => {},
});

const GlobalProvider = ({children}) => {
    const [userid, setUserid] = useState("");
    const [user, setUser] = useState(null);
    const [isLogged, setIsLogged] = useState(false)

    const handleUserId = async (userId) => {
        try {
            if (userId) {
                setUserid(userId);
                const apiUser = await getUser(userId);
                setUser(apiUser);
              }
        } catch (error) {
            Alert.alert(error.message)   
        }
      };
        
    const checkUser = async () => {
        const localId = await AsyncStorage.getItem(UserIdKey);
        await handleUserId(localId);
    };

    const login = async (email, password) => {
        try {
            const userCred = await signUserIn(email, password);
            if (userCred && userCred.user) {
                await handleUserId(userCred.user.uid);
                setIsLogged(true)
                AsyncStorage.setItem(UserIdKey, userCred.user.uid);
                Toast.showWithGravity('Connecté !', Toast.LONG, Toast.TOP )
            }
        } catch (error) {
            Alert.alert(error.message)
        }
    };

    const updateUserProfile = async (args, image) => {
        let newUser = Object.assign( {}, user[0], {...args});
        newUser.accountFull = true
        createImage({
            data: image.image.base64,
            type: "image/jpeg",
            name: "profile.png",
            path: user[0].userId+"/profile/",
        })
        updateUser(newUser);  
        setUser([newUser]);
        return true
    } 

    const register = async (username, artisteName, email, password) => {
        try {
            const userCred = await signUserUp(email, password);
            if (userCred && userCred.user) {
                createUser({
                    userId : userCred.user.uid,
                    username: username,
                    artisteName: artisteName,
                    email: email,
                    accountFull: false,
                    oeuvres: [],
                    chat: [],
                    networks: []
                });
                AsyncStorage.setItem(UserIdKey, userCred.user.uid);
                Toast.showWithGravity('Compte créé', Toast.LONG, Toast.TOP )
            }
        } catch (error) {
            Alert.alert(error.message)
        }
    };

    const logout = () => {
        signUserOut();
        setUserid("");
        setIsLogged(false)
        AsyncStorage.removeItem(UserIdKey);
    };

    const modifyUser = (newUserProps, sendToServer = true) => {
        if (!userid) {
        console.error("User ID is not there yet!");
        return;
        }
        const newUser = { ...user, ...newUserProps };
        setUser(newUser);
        if (sendToServer) updateUser({ id: userid, ...newUserProps });
    };

    const AddWork = async (name, artistFullName, type, categories, description, creationYear, dimension, image) => {
        try {
            const id = uuid.v4()
            createWork({
                workId: id,
                WorkName: name,
                ArtistName: artistFullName,
                WorkType: type,
                WorkCategories: categories.split(","),
                WorkPictures: ["lol"],
                userId : userid,
                WorkDimension: dimension,
                WorkCreationYear: creationYear,
                WorkDescription: description, 
                WorkLikes: 0,
                WorkMessages: 0
            });
            createImage({
                data: image.image.base64,
                type: "image/jpeg",
                name: `${id}.png`,
                path: user[0].userId+"/works/",
            })
            Toast.showWithGravity('Annonce créé', Toast.LONG, Toast.TOP )
        } catch (error) {
            
            Alert.alert(error.message)
        }
    };

    return <GlobalContext.Provider value={{ 
        user,
        isLogged,
        register,
        logout,
        login,
        updateUserProfile,
        userid,
        AddWork
     }}>
            {children}
        </GlobalContext.Provider>
}

export default GlobalProvider