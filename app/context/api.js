import { Alert } from "react-native";

const getData = async (url) => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (err) {
    Alert.alert(
      "Erreur lors de la récupération des données",
      err.message
    );
  }
};

const sendData = async (url, data, method) => {
  try {
    const response = await fetch(url, {
      method: method,
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    });
    const result = await response.json();
    return result;
  } catch (err) {
    ;
    Alert.alert(
      "Erreur lors de la mise à jour des données",
      err.message
    );
  }
};

export const getUser = async (userId) =>
  getData(
    `https://europe-west1-gale-airy.cloudfunctions.net/user?userId=${userId}`
  );

export const updateUser = async (userFields) =>
  sendData(
    `https://europe-west1-gale-airy.cloudfunctions.net/user?userId=${userFields.userId}`,
    userFields,
    "PATCH"
  );

export const createUser = async (userFields) =>
sendData(
  `https://europe-west1-gale-airy.cloudfunctions.net/user?userId=${userFields.userId}`,
  userFields,
  "POST"
);

export const createWork = async (workFields) =>
  sendData(
    `https://europe-west1-gale-airy.cloudfunctions.net/work?workId=${workFields.workId}`,
    workFields,
    "POST"
);

export const getWork = async (userId) =>
  getData(
    `https://europe-west1-gale-airy.cloudfunctions.net/work?userId=${userId}`
  );

  export const createImage = async (imageFields) =>
  sendData(  
    `https://europe-west1-gale-airy.cloudfunctions.net/image`,
    imageFields,
    "POST"
  )

  export const getImage = async (userId, folder, name) =>
  getData(
    `https://europe-west1-gale-airy.cloudfunctions.net/image?userId=${userId}&folder=${folder}&name=${name}`
  );

  export const getAllWorks = async () => 
    getData(
      'https://europe-west1-gale-airy.cloudfunctions.net/work'
    );