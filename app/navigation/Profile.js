import * as React from 'react';
import { useContext, useState, useEffect } from "react"
import { ScrollView, Text, View, StyleSheet, Button, KeyboardAvoidingView, Image, TouchableOpacity } from 'react-native';
import { GlobalContext } from '../context/AppData';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Divider, TextInput, Title } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import Toast from 'react-native-simple-toast'
import { getImage } from '../context/api';
import { Buffer } from "buffer"
import noImage from "../assets/emptyAccountImage.png"
import { getWork } from '../context/api';
import Posts from '../components/Posts';

const AppStack = createNativeStackNavigator();

function settingProfile({ navigation }) {
  const { user, updateUserProfile } = useContext(GlobalContext)
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState(null)
  

  const addImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      base64: true,
      aspect: [4, 3],
      quality: 1,
    })
    setImage(result)
  }

  const handleCamera = async () => {
    const permissionResult = await ImagePicker.requestCameraPermissionsAsync();
    let result = await ImagePicker.launchCameraAsync({
      base64: true
    })
    setImage(result)
  }

  const handleUpdateProfile = async () => {
    if (firstName !== '' && lastName != '' && image != null && description != "") {
      const resUpdate = await updateUserProfile({firstName: firstName, lastName: lastName, description: description}, {type: 'profile', image: image })
      resUpdate == true ? Toast.showWithGravity('Profil mis à jour.', Toast.LONG, Toast.TOP ) : Toast.showWithGravity('Échec de la mise à jour du profil.', Toast.LONG, Toast.TOP )
    } else {
      Toast.showWithGravity('Impossible de mettre à jour votre profil.', Toast.LONG, Toast.TOP )
    }
  }

  // UPLOAD PHOTO ICI
  // uploadImageToStorage(result.uri, `profilePicture-${user[0].userId}`)
  const fillInput = () => {
    setFirstName(user[0].firstName)
    setLastName(user[0].lastName)
    setDescription(user[0].description)
    setImage(user[0].image)
  }

  useEffect(()=>{
    fillInput();
  }, [])
  return (
    <View style={styles.main}>
      <Title>Finaliser votre compte</Title>
      <KeyboardAvoidingView
        behavior="padding"
        >
        <View>
          <TextInput
              placeholder="Prénom"
              value={firstName}
              onChangeText={text => setFirstName(text)}
              style={styles.input}
            />
          <TextInput
            placeholder="Nom"
            value={lastName}
            onChangeText={text => setLastName(text)}
            style={styles.input}
          />
          <TextInput
            multiline
            numberOfLines={10}
            placeholder="Description"
            value={description}
            onChangeText={text => setDescription(text)}
            style={styles.input}
          /> 
          <Text>Upload photo de profil</Text>
          <View style={{ 
            flex: 1,
            width: '100%',
            height: 100,
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
            paddingBottom: 15,
            marginTop: 35,
           }}>

            <TouchableOpacity style={styles.buttonImage} onPress={addImage}>
              <Text>Galerie</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonImage} onPress={handleCamera}>
              <Text>Camera</Text>
            </TouchableOpacity>
          </View>
        </View>

        <Divider></Divider>
          <TouchableOpacity style={styles.button} onPress={()=> {
            handleUpdateProfile()
            navigation.goBack()
          }}>
            <Text>Mettre à jour</Text>
          </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
    )
}

function mainProfile({ navigation }) {
  const { user } = useContext(GlobalContext)
  const [avatarSrc, setAvatarSrc] = useState("");
  const forceUpdate = useState()[1].bind(null, {})
  const [allposts, setAllposts] = useState('');

  const getPosts = () => {
    getWork(user[0].userId).then((res_array) => {
      setAllposts(res_array.map((elem) => (
        <Posts
          key={elem.workId}
          id={elem.workId}
          type={elem.WorkType == '' ? null : elem.WorkType}
          title={elem.WorkName == '' ? null : elem.WorkName}
          creation_date={elem.WorkCreationYear == '' ? null : elem.WorkCreationYear}
          artist={elem.ArtistName == '' ? null : elem.ArtistName}
          dimensions={elem.WorkDimension == '' ? null : elem.WorkDimension}
          description={elem.WorkDescription == '' ? null : elem.WorkDescription}
          likes_num={elem.WorkLikes}
          chats_num={elem.WorkMessages}
          userid = {elem.userId}
          categories = {elem.WorkCategories.join(", ")}
        />
        )));
    });
  }

  useEffect(async () => {
    getPosts()
    try {      
      let dataImage = await getImage(user[0].userId, 'profile','profile.png');
      setAvatarSrc('data:image/png;base64,'+ new Buffer.from(dataImage[0].data).toString('base64'))
    } catch (error) {
      setAvatarSrc('')
    }
  }, []);

  return (
    <ScrollView>
    <View style={styles.main}>
      <Button 
          style={styles.buttonAlert}
          onPress={() => {
            navigation.navigate('settingProfile')
          }}
          title="Modifer profil"
          color="#F0E68C"/>
      <View style={styles.dataProfile}>
        <Image 
          style={styles.image}
          source={avatarSrc == '' ? noImage : { uri: avatarSrc }} 
        />
        <View style={styles.textProfile}>
        <Text>{user[0].username != undefined ? user[0].username : 'PAS DE PSEUDO'}</Text>
        <View style={{ 
            flex: 1,
            flexDirection: 'row'
           }}>
            <Text>Nom d'artiste : </Text>
            <Text style={{ fontWeight: "bold" }}>{user[0].artisteName}</Text>
        </View>
        <Text>{user[0].description == null || user[0].description == undefined ? "" : user[0].description }</Text>
        </View>
      </View>
      <View style={styles.oeuvreEmpty}>
            <ScrollView style={styles.scroll_view}>
            {allposts == '' ? <Text style={{ color: "#9a9a9a" }}>Vous n'avez pas encore d'art à exposer</Text> : allposts}
            </ScrollView>
      </View>
    </View>
    </ScrollView>
  )
}

export default function Profile() {

  return (
  <NavigationContainer independent={true}>
    <AppStack.Navigator 
      screenOptions={{ headerShown: false }}
      initialRouteName='mainProfile'>
      <AppStack.Screen name="mainProfile" component={mainProfile} />
      <AppStack.Screen name="settingProfile" component={settingProfile}  />
    </AppStack.Navigator>
  </NavigationContainer>
  )
}

const styles = StyleSheet.create({

  oeuvreEmpty: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50
  },
  dataProfile: {
    padding: 20,
    flexDirection: "row",
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  image: {
    width: 100,
    height: 100,
    borderRadius: 100/ 2,
    borderWidth: 1,
    borderColor: '#000000',
  },

  textProfile: {
    width: '60%',
    paddingTop: 10,
    height: '100%',
  },

  buttonAlert: { 
    flex: 1,
    backgroundColor: "black",
    justifyContent: 'center',
    marginHorizontal: 20,
  },

  main: {
    flex: 1,
    padding: 20,
    flexDirection: "column"
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  inputContainer: {
    width: '80%'
  },

  input: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
  },

  buttonContainer: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },

  button: {
    backgroundColor: '#F0E68C',
    width: '100%',
    padding: 15,
    marginTop: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  
  buttonImage: {
    backgroundColor: '#F0E68C',
    width: '45%',
    padding: 15,
    height: 50,
    borderRadius: 10,
    textAlign: 'center'
  },

  buttonOutline: {
    backgroundColor: 'white',
    marginTop: 5,
    borderColor: '#0782F9',
    borderWidth: 2,
  },

  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },

  buttonOutlineText: {
    color: '#0782F9',
    fontWeight: '700',
    fontSize: 16,
  },
});
