import React, { useState, useContext } from 'react'
import { GlobalContext } from '../context/AppData';
import { KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { Card } from 'react-native-paper';



const Register = () => {
    const [username, setUsername] = useState('')
    const [artisteName, setArtisteName] = useState("")
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordVerication, setPasswordVerification] = useState('')
    const { register, isLogged } = useContext(GlobalContext)
  
    const handleSignUp = () => {
      if (password === passwordVerication && password !== '' && passwordVerication !== '') {
        register(username, artisteName, email, password)
      }
    }
    
    return (
    <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center',}}>
    <Card 
    mode = "outlined"
    style={styles.card}
    >
    <Card.Title title="Inscription" titleStyle={styles.text}/>
    <Card.Content>
      <View>
        <TextInput
            placeholder="Pseudo"
            value={username}
            onChangeText={text => setUsername(text)}
            style={styles.input}
          />
        <TextInput
            placeholder="Nom d'artiste"
            value={artisteName}
            onChangeText={text => setArtisteName(text)}
            style={styles.input}
          />
        <TextInput
          placeholder="Email"
          value={email}
          onChangeText={text => setEmail(text)}
          style={styles.input}
          autoCapitalize = 'none'
          keyboardType="email-address"
        />
        <TextInput
          placeholder="Mot de passe"
          value={password}
          onChangeText={text => setPassword(text)}
          style={styles.input}
          autoCapitalize = 'none'
          secureTextEntry
        />
        
        <TextInput
          placeholder="Confirmation du mot de passe"
          value={passwordVerication}
          onChangeText={text => setPasswordVerification(text)}
          style={styles.input}
          autoCapitalize = 'none'
          secureTextEntry
        />
      </View>
        <TouchableOpacity
          onPress={handleSignUp}
          style={styles.buttonSend}
        >
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
      </Card.Content>
    </Card>
    </KeyboardAvoidingView>
    )
}

export default Register

const styles = StyleSheet.create({
  text: {
    display: "flex",
    justifyContent: 'center',
    alignContent: 'center',
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
    borderColor : "lightgray", 
    borderWidth: 1, 
    width: 300
  },
  buttonSend: {
    width: '100%',
    padding: 15,
    borderRadius: 10,
    backgroundColor: "#F0E68C",
    alignItems: 'center',
    marginTop: 20,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
  card:{
    display:"flex" ,
    alignItems:"center",
    width : "80%", 
    marginLeft : "10%",
    marginRight: "10%"
  }
})