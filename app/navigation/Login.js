import React, { useState, useContext } from 'react'
import { GlobalContext } from '../context/AppData';
import { KeyboardAvoidingView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Register from './Register';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Card } from 'react-native-paper';


const AppStack = createNativeStackNavigator();

const LoginComponent = ({ navigation }) => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const { login, isLogged } = useContext(GlobalContext)

  const handleSignIn = async () => {
    await login(email, password)
  }
  
  return (
    <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center',}}>
    <Card 
    mode = "outlined"
    style={styles.card}
    >
    <Card.Title title="GaleAiry Login" />
    <Card.Content>
    <View style={styles.inputContainer}>
      <TextInput
        placeholder="Email"
        value={email}
        onChangeText={text => setEmail(text)}
        style={styles.input}
        keyboardType="email-address"
        autoCapitalize = 'none'
      />
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
        style={styles.input}
        autoCapitalize = 'none'
        secureTextEntry
      />
    </View>
      <TouchableOpacity
        onPress={handleSignIn}
        style={styles.buttonSend}
      >
      <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('Register')}
        style={[styles.buttonSend]}
      >
        <Text style={styles.buttonText}>Register</Text>
      </TouchableOpacity>
    </Card.Content>
  </Card>
  </KeyboardAvoidingView>
  )
}

const Login = ({navigation}) => {
  return (
    <NavigationContainer independent={true}>
      <AppStack.Navigator>
        <AppStack.Screen name="Login" component={LoginComponent} />
        <AppStack.Screen name="Register" component={Register}/>
      </AppStack.Navigator>
    </NavigationContainer>
  )
}

export default Login

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    width: '80%'
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
    borderColor : "lightgray", 
    borderWidth: 1, 
    width: 200
  },
  buttonSend: {
    width: '100%',
    paddingVertical: 8,
    borderRadius: 10,
    backgroundColor: "#F0E68C",
    alignItems: 'center',
    marginTop: 15
  },
  buttonOutline: {
    backgroundColor: 'white',
    marginTop: 5,
    borderColor: '#0782F9',
    borderWidth: 2,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
  buttonOutlineText: {
    color: '#0782F9',
    fontWeight: '700',
    fontSize: 16,
  },
  card:{
    display:"flex" ,
    alignItems:"center",
    width : "66%", 
    marginLeft : "16%",
    marginRight: "16%"
  },
  buttonBack: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: "red",
  }
})