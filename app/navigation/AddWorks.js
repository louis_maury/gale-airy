import React, {useState, useContext} from 'react';
import { TextInput, View, StyleSheet, Text, KeyboardAvoidingView, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-paper';
import * as ImagePicker from 'expo-image-picker';
import { GlobalContext } from '../context/AppData';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const AddWorks = ({navigation}) => {
  const [name, setName] = useState('')
  const [artistFullName, setArtistFullName] = useState('')
  const [workType, setWorkType] = useState('')
  const [categories, setCategories] = useState('')
  const [picture, setPicture] = useState(null)
  const [dimension, setDimension] = useState('')
  const [creationDate, setCreationDate] = useState('')
  const [description, setDescription] = useState('')
  const { AddWork } = useContext(GlobalContext)

  const handleCreateWork = () => {
    AddWork(name, artistFullName, workType, categories, description, creationDate, dimension, {type: 'works', image: picture })
  }

  const clearInput = () => {
    setArtistFullName("");
    setName("");
    setWorkType("");
    setCategories([])
    setDimension("")
    setCreationDate("")
    setDescription("")
  }

  const addImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      base64: true,
      aspect: [16, 24],
      quality: 1,
    })
    setPicture(result)
  }

  return (
    <KeyboardAwareScrollView>
    <Card 
    mode = "outlined"
    style={styles.card}
    >
    <Card.Title title="Ajouter une annonce" />
    <Card.Content>
      <KeyboardAvoidingView
        behavior="padding"
        >
        <View>
          <TextInput
              placeholder="Nom de l'oeuvre"
              value={name}
              onChangeText={text => setName(text)}
              style={styles.input}
            />
          <TextInput
            placeholder="Nom complet de l'artiste"
            value={artistFullName}
            onChangeText={text => setArtistFullName(text)}
            style={styles.input}
          />
          <TextInput
            placeholder="Type d'oeuvre"
            value={workType}
            onChangeText={text => setWorkType(text)}
            style={styles.input}
          />
          <TextInput
            placeholder="Dimension"
            value={dimension}
            onChangeText={text => setDimension(text)}
            style={styles.input}
          />
          <TextInput
            placeholder="Année de création"
            value={creationDate}
            onChangeText={text => setCreationDate(text)}
            style={styles.input}
          />
          <TextInput
            placeholder="Description"
            value={description}
            onChangeText={text => setDescription(text)}
            style={styles.input}
            multiline
            numberOfLines={6}
          />
          <TextInput
            placeholder="Catégorie"
            value={categories}
            onChangeText={text => setCategories(text)}
            style={styles.input}
            autoCapitalize = 'none'
          />
          <TouchableOpacity style={styles.buttonGal} onPress={addImage}>
            <Text>Upload image</Text>
          </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonSend}
        onPress={() => {
          handleCreateWork()
          clearInput()
          navigation.goBack()
        }
      }
      >
        <View>
        <Text>Ajouter l'annonce</Text>
        </View>
      </TouchableOpacity>
      </View>
      </KeyboardAvoidingView>
    </Card.Content>
  </Card>
  </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputContainer: {
    width: '80%'
  },
  input: {
    backgroundColor: 'white',
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 5,
    borderColor : "lightgray", 
    borderWidth: 1, 
    width: 200
  },
  buttonContainer: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  buttonSend: {
    width: '100%',
    padding: 15,
    borderRadius: 10,
    backgroundColor: "#F0E68C",
    alignItems: 'center',
    marginTop: 20,
  },
  buttonGal: {
    width: '75%',
    padding: 15,
    borderRadius: 10,
    backgroundColor: "#F0E68C",
    alignItems: 'center',
    marginTop: 5,
    marginLeft: 20
  },
  buttonOutline: {
    backgroundColor: 'white',
    marginTop: 5,
    borderColor: '#0782F9',
    borderWidth: 2,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
  buttonOutlineText: {
    color: '#0782F9',
    fontWeight: '700',
    fontSize: 16,
  },
  card:{
    display:"flex" ,
    alignItems:"center",
    width : "66%", 
    marginLeft : "16%",
    marginRight: "16%"
  }
})

export default AddWorks