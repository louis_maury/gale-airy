import React, { useContext } from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {TouchableOpacity, Text, StyleSheet, View} from "react-native"
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// You can import from local files
import Home from './Home';
import Favorite from './Favorite';
import AddWorks from './AddWorks';
import Tchat from './Tchat';
import Profile from './Profile';
import Login from './Login';
import { GlobalContext } from '../context/AppData';

const Tab = createBottomTabNavigator();

export default function Tabs() {
    const { isLogged, logout } = useContext(GlobalContext)
    
    return (
        <Tab.Navigator 
         screenOptions={{ headerShown: isLogged ? true: false }}>
            <Tab.Screen name="Accueil" component={ Home } 
                options={{ 
                    tabBarIcon: ({ color, size }) => (
                        <MaterialCommunityIcons name='home' color={color} size={26} />
                    )
                }}/>
                
            <Tab.Screen name="Favoris" component={isLogged == true ? Favorite : Login}
                options={{ 
                    tabBarIcon: ({ color, size}) => (
                        <MaterialCommunityIcons name='heart' color={color} size={26} />
                    )
                }}/>
        
            <Tab.Screen name="Ajouter" component={isLogged == true ? AddWorks : Login}
                    options={{ 
                        tabBarIcon: ({ color, size}) => (
                            <MaterialCommunityIcons name='plus' color={color} size={26} />
                        )
                    }}/>
            
            <Tab.Screen name="Messages" component={isLogged == true ? Tchat : Login}
                    options={{ 
                        tabBarIcon: ({ color, size}) => (
                            <MaterialCommunityIcons name='chat' color={color} size={26} />
                        )
                    }}/>
            
            <Tab.Screen name="Compte" component={isLogged == true ? Profile : Login}
                    options={{ 
                        headerRight: () => (
                            <View style={{paddingRight:10}}>
                            <TouchableOpacity onPress={logout} style={styles.logoutButton}>
                                <Text>Logout</Text>
                            </TouchableOpacity>
                            </View>
                          ),
                        tabBarIcon: ({ color, size}) => (
                            <MaterialCommunityIcons name='account' color={color} size={26} />
                        )
                    }}/>
        </Tab.Navigator>
    );
}

const styles = StyleSheet.create({
    logoutButton: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: "#F0E68C",
        alignItems: 'center',
        paddingHorizontal: 10
    },
  });