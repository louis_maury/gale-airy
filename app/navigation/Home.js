import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { GlobalContext } from '../context/AppData';

//customs
import Posts from '../components/Posts';
import { getWork } from '../context/api';

export default Home = ({navigation}) => {
  const [allposts, setAllposts] = useState('');

  const getPosts = () => {
    getWork("all").then((res_array) => {
      setAllposts(res_array.map((elem) => (
        <Posts
          key={elem.workId}
          id={elem.workId}
          type={elem.WorkType == '' ? null : elem.WorkType}
          title={elem.WorkName == '' ? null : elem.WorkName}
          creation_date={elem.WorkCreationYear == '' ? null : elem.WorkCreationYear}
          artist={elem.ArtistName == '' ? null : elem.ArtistName}
          dimensions={elem.WorkDimension == '' ? null : elem.WorkDimension}
          description={elem.WorkDescription == '' ? null : elem.WorkDescription}
          likes_num={elem.WorkLikes}
          chats_num={elem.WorkMessages}
          userid = {elem.userId}
          categories = {elem.WorkCategories.join(", ")}
        />
        )));
    });
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getPosts();
    });

    return unsubscribe;
  }, [navigation]);
    

  return (
    <View style={styles.container}>
      <ScrollView style={styles.scroll_view}>
        {allposts == '' ? null : allposts}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  scroll_view: {
    marginLeft: 22.5,
  },
  paragraph: {
    margin: 24,
    marginTop: 0,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});