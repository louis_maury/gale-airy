import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';

export default function Tchat() {
  return (
    <View style={styles.container}>
      <Text style={styles.paragraph}>
      🚧 WIP Tchat 🚧
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 24,
  },
  paragraph: {
    margin: 24,
    marginTop: "50%",
    fontSize: 42,
    fontWeight: 'bold',
    textAlign: 'center',
    width: "150%"
  }
});
