import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';

// Import context
import GlobalProvider from './context/AppData';

// You can import from local files
import Tabs from './navigation/Tabs';

export default function App() {
  return (
      <GlobalProvider>
        <NavigationContainer>
          <Tabs/>
        </NavigationContainer>
      </GlobalProvider>
  );
}