import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { getImage, getUser } from '../context/api';
import { Dimensions } from 'react-native';
import { Buffer } from "buffer"

dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - 100
}

const styles = StyleSheet.create({
    card_style: {
        marginTop: 50,
        height: 200,
        width: 380,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: 'white',
    },
    z_card_style: {
        marginTop: 50,
        height: dimensions.height,
        width: "100%",
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    multiple_pics: {
        flexDirection: 'row',
    },
    image_style: {
        height: 200,
        width: 150,
    },
    z_image_style: {
        height: dimensions.height / 2 - 20,
        width: "60%",
    },
    art_card: {
        height: 200,
        width: 210,
        justifyContent: 'space-around',
        marginRight: 10,
        marginLeft: 10,
    },
    z_art_card: {
        height: dimensions.height / 2 - 20,
        width: 380,
        justifyContent: 'space-around',
        marginRight: 10,
        marginLeft: 10,
    },
    title_style: {
        display: 'flex',
        flexDirection: 'row',
    },
    z_title_style: {
        display: 'flex',
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 10,
    },
    artist_style: {
        fontStyle: 'italic',
    },
    z_artist_style: {
        fontStyle: 'italic',
        marginRight: 10,
        marginLeft: 10,
    },
    z_description_style: {
        marginRight: 10,
        marginLeft: 10,
    },
    type_style: {
        opacity: 0.5,
    },
    z_type_style: {
        opacity: 0.5,
        marginRight: 10,
        marginLeft: 10,
    },
    z_dimensions_style: {
        marginLeft: 10,
        marginRight: 10,
    },
    likes_chats_style: {
        color: '#000000',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
    },
    elem_likes_chats: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: 5,
    },
    usernameStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-start',
        top: "15%"
    },
})


const Posts = (props) => {
    const [isZoom, setIsZoom] = useState(false)
    const [pictures, setPictures] = useState('null')
    const [user, setUser] = useState(null)

    const zoomToggle = () => {
        setIsZoom(!isZoom);
    }

    const onPress = () => {
        zoomToggle()
    }

    useEffect(async () => {
        try {      
            const apiUser = await getUser(props.userid);
            setUser(apiUser);
            let dataImage = await getImage(props.userid, 'works',`${props.id}.png`);
            setPictures('data:image/png;base64,'+ new Buffer.from(dataImage[0].data).toString('base64'))
        } catch (error) {
            console.log(error)
        }
      }, []);
    
    return (
        <TouchableOpacity onPress={onPress} style={isZoom ? styles.z_card_style : styles.card_style}>
            { isZoom ?
            <View style={styles.multiple_pics}>
                <Image style={styles.z_image_style} source={{ uri: pictures }} />
            </View>
            :
            <Image style={styles.image_style} source={{ uri: pictures }} />}
            <View style={isZoom ? styles.z_art_card : styles.art_card}>
                <View style={isZoom ? styles.z_title_style : styles.title_style}>
                    <Text>{props.title}{
                    props.creation_date == null 
                        ?
                    <Text></Text>
                        :
                    <Text> - {props.creation_date}</Text> }</Text>
                </View>
                <Text style={isZoom ? styles.z_artist_style : styles.artist_style}>{props.artist}</Text>                
                <View>
                    <Text style={isZoom ? styles.z_description_style : styles.description_style}>"{props.description}"</Text>
                </View>
                <View style={isZoom ? styles.z_type_style : styles.type_style}>
                    <Text>{props.type}</Text>
                    <Text style={isZoom ? styles.z_dimensions_style : styles.dimensions_style}>{props.dimensions}</Text>
                </View>
                {isZoom ? 
                <View style={styles.z_type_style}>
                    <Text>{props.categories}</Text>
                </View>
: null }

                <View style={isZoom ? styles.z_type_style : styles.usernameStyle}>
                    <Text>{user != null ? user[0].artisteName : ""}</Text>
                </View>
                <View style={styles.likes_chats_style}>
                    <View style={styles.elem_likes_chats}>
                        <Text>{props.likes_num}</Text><MaterialCommunityIcons name='heart' color='red' size={26} />
                    </View>
                    <View style={styles.elem_likes_chats}>
                        <Text>{props.chats_num}</Text><MaterialCommunityIcons name='chat' color='black' size={26} />
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default Posts;