import { initializeApp, getApps, getApp } from "firebase/app";
import { getStorage, ref, uploadBytes, getDownloadURL, getBytes, getBlob } from "firebase/storage"
import {
  signInWithEmailAndPassword,
  signOut,
  getAuth,
  createUserWithEmailAndPassword,
} from "firebase/auth";
import Toast from 'react-native-simple-toast'

let firebaseApp = ''

export const firebaseConfig = {
    apiKey: "AIzaSyB1IBhk2aXHVr-LNCFYZu2yAm8734YYIK8",
    authDomain: "gale-airy.firebaseapp.com",
    projectId: "gale-airy",
    storageBucket: "gale-airy.appspot.com",
    messagingSenderId: "605382076148",
    appId: "1:605382076148:web:de1dc1f588a1528abe60a1",
    measurementId: "G-0LHNL61CSC"
  };

if (getApps().length === 0) {
  firebaseApp = initializeApp(firebaseConfig);
} else {
  firebaseApp = getApp()
}

export const signUserIn = async (login, password) =>
signInWithEmailAndPassword(getAuth(), login, password)
.catch((err) =>
  Toast.showWithGravity('Authentification échouée - Vos identifiants n\'ont pas été reconnus', Toast.LONG, Toast.TOP )
);

export const signUserUp = async (email, password) => {
  let res = await createUserWithEmailAndPassword(getAuth(), email, password)
  return res
}

export const signUserOut = async () => {
  await signOut(getAuth())
  .then(e => { Toast.showWithGravity('Deconnection', Toast.LONG, Toast.TOP ) })
  .catch(err => { Toast.showWithGravity('Echec de deconnexion', Toast.LONG, Toast.TOP ) })
};
